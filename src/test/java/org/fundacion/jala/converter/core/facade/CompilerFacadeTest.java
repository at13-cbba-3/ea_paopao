package org.fundacion.jala.converter.core.facade;

import org.fundacion.jala.converter.core.exceptions.CompilerException;
import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class CompilerFacadeTest {

    @Ignore
    @Test
    public void facadeJavaCompile() throws CompilerException {
        assertTrue("".equals(CompilerFacade.facadeJavaCompile("")));
    }
    @Ignore
    @Test
    public void facadePythonCompile() throws CompilerException {
        assertEquals("", CompilerFacade.facadePythonCompile(""));
    }
    @Ignore
    @Test
    public void facadeNodejsCompile() throws CompilerException {
        assertEquals("", CompilerFacade.facadeNodejsCompile(""));
    }
}