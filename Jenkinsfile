pipeline {
  agent any
  environment{
    DOCKER_HUB_PASSWORD = credentials('docker_hub_pass')
    SONAR_TOKEN = credentials('sonar_token')
    shortCommit = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
    emailsTeam = credentials('emails')
    console = 'console'
  }
  stages {
    stage('commit') {
      steps {
        sh './gradlew clean build'
      }
      post {
        always {
            archiveArtifacts artifacts: 'build/libs/*.jar', fingerprint: true
            junit 'build/test-results/test/*.xml'
        }
      }
    }
    stage('code quality') {
      steps {
        sh './gradlew sonarqube'
      }
    }
    stage('package') {
      steps {
        sh 'docker build -t at13:1.0 .'
      }
    }
    stage('publish') {
      steps {
        sh 'docker login -u ${DOCKER_HUB_PASSWORD_USR} -p ${DOCKER_HUB_PASSWORD_PSW}'
        sh 'docker image tag at13:1.0 edsonarios/at13:1.0'
        sh 'docker push edsonarios/at13:1.0'
      }
    }
    stage('deploy') {
      steps {
        sh 'docker-compose -f ./docker-compose.yml up -d'
      }
    }
  }
  post {
    always {
          mail to: emailsTeam,
          subject: "Build #${currentBuild.number}, with Status: ${currentBuild.result}",
          body: " Build: #${currentBuild.number} \n CommitID: ${shortCommit} \n Log: ${currentBuild.absoluteUrl + console}"
        }
  }
}
