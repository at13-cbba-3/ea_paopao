FROM openjdk:11
EXPOSE 8080
WORKDIR /app
COPY ./build/libs/*.jar /app/converter-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java", "-jar", "converter-0.0.1-SNAPSHOT.jar" ]
